package org.medida.inhalerdetection;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import static org.medida.inhalerdetection.InhalerDetectionActivity.*;
import static org.medida.inhalerdetection.InhalerDetectionActivity.EXTRA_INPUT_INHALER_TYPE_ENUM;
import static org.medida.inhalerdetection.R.drawable.default_outline;
import static org.medida.inhalerdetection.R.drawable.diskus1024;
import static org.medida.inhalerdetection.R.drawable.diskus1024_overlay;
import static org.medida.inhalerdetection.R.drawable.ellipta1024;
import static org.medida.inhalerdetection.R.drawable.ellipta1024_overlay;
import static org.medida.inhalerdetection.R.drawable.flutiform1024;
import static org.medida.inhalerdetection.R.drawable.flutiform1024_overlay;
import static org.medida.inhalerdetection.R.drawable.novolizer1024;
import static org.medida.inhalerdetection.R.drawable.novolizer1024_overlay;
import static org.medida.inhalerdetection.R.drawable.spiromax1024;
import static org.medida.inhalerdetection.R.drawable.spiromax1024_overlay;
import static org.medida.inhalerdetection.R.drawable.turbohaler1024;
import static org.medida.inhalerdetection.R.drawable.turbohaler1024_overlay;

public class PreDetectionActivity extends Activity {

    final int DETECTION_CODE = 100;

    private static final String    TAG = "OCVSample::Activity";

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 300;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 400;
    private static final int MY_PERMISSIONS_REQUEST_BOTH = 500;

    private boolean isCameraPermitted = true;
    private boolean isStoragePermitted = true;

    private ImageSwitcher switcher;
    private int switchPeriod = 1500; //in milisseconds

    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_detection);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/morebi_rounded_regular.ttf");

        Button button = (Button) findViewById(R.id.continueButton);

        button.setText(R.string.start_button_text);
        button.setTypeface(font);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                if(!CheckPermissions())
                    RequestPermissions();
                else
                    AdvanceToInhalerDetection();

            }
        });

        UpdateImageView();
    }

    private void AdvanceToInhalerDetection()
    {
        Intent receivedIntent = getIntent();
        Intent newIntent = new Intent(getBaseContext(), InhalerDetectionActivity.class);

        Bundle oldBundle = receivedIntent.getExtras();
        newIntent.putExtras(oldBundle);

        startActivityForResult(newIntent, DETECTION_CODE);
    }

    private void UpdateImageView() {

        switcher = (ImageSwitcher) findViewById(R.id.inhalerImageSwitcher);
        switcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                myView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                myView.setAdjustViewBounds(true);
                myView.setLayoutParams(new ImageSwitcher.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
                return myView;
            }
        });

        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        switcher.setInAnimation(in);
        switcher.setOutAnimation(out);

        View root = switcher.getRootView();
        root.setBackgroundColor(getResources().getColor(android.R.color.white));


        Intent intent = getIntent();

        InhalerType inhalerType = (InhalerType) intent.getSerializableExtra(EXTRA_INPUT_INHALER_TYPE_ENUM);

        TextView text = (TextView) findViewById(R.id.instructionTextView);
        text.setText(getString(R.string.pre_rec_instructions));

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/morebi_rounded_regular.ttf");
        text.setTypeface(font);

        Button button = (Button) findViewById(R.id.continueButton);
        button.setTypeface(font);

        int overlayOffImg, overlayOnImg;

        switch(inhalerType)
        {
            case Unknown:
                //image.setImageDrawable(null);
                text.setText(getString(R.string.pre_rec_instruction_notemplate));
                overlayOffImg = overlayOnImg = -1;
                switcher.setImageResource(default_outline);
                //image.setMaxHeight(0);
                break;

            case Diskus:
                //image.setImageResource(R.drawable.diskus1024);
                overlayOffImg = diskus1024;
                overlayOnImg = diskus1024_overlay;
                break;
            case Ellipta:
                //image.setImageResource(R.drawable.ellipta1024);
                overlayOffImg = ellipta1024;
                overlayOnImg = ellipta1024_overlay;
                break;
            case Flutiform:
                //image.setImageResource(R.drawable.flutiform1024);
                overlayOffImg = flutiform1024;
                overlayOnImg = flutiform1024_overlay;
                break;
            case Novoziler:
                //image.setImageResource(R.drawable.novolizer1024);
                overlayOffImg = novolizer1024;
                overlayOnImg = novolizer1024_overlay;
                break;
            case Spiromax:
                //image.setImageResource(R.drawable.spiromax1024);
                overlayOffImg = spiromax1024;
                overlayOnImg = spiromax1024_overlay;
                break;
            case Turbohaler:
                //image.setImageResource(R.drawable.turbohaler1024);
                overlayOffImg = turbohaler1024;
                overlayOnImg = turbohaler1024_overlay;
                break;

            case KHaller:
                overlayOffImg = R.drawable.khaler1024;
                overlayOnImg = R.drawable.khaler1024_overlay;
                break;

            case Easyhaler:
                overlayOffImg = R.drawable.easyhaler1024;
                overlayOnImg = R.drawable.easyhaler1024_overlay;
                break;

            case NextHaler:
                overlayOffImg = R.drawable.nexthaler1024;
                overlayOnImg = R.drawable.nexthaler1024_overlay;
                break;

            case Seretide:
                overlayOffImg = R.drawable.seretide1024;
                overlayOnImg = R.drawable.seretide1024_overlay;
                break;

            case Twisthaler:
                overlayOffImg = R.drawable.twisthaler1024;
                overlayOnImg = R.drawable.twisthaler1024_overlay;
                break;

            default:
                //image.setImageResource(R.drawable.icon);
                overlayOffImg = overlayOnImg = -1;
                break;
        }

        final int _overlayOffImg = overlayOffImg;
        final int _overlayOnImg = overlayOnImg;

        if(overlayOffImg > -1)
        {
            switcher.setImageResource(_overlayOffImg);
            switcher.postDelayed(new Runnable() {
                int i = 0;
                public void run() {
                    switcher.setImageResource(
                            i++ % 2 == 1 ?
                                    _overlayOffImg :
                                    _overlayOnImg);
                    switcher.postDelayed(this, switchPeriod);
                }
            }, switchPeriod);
        }

        ImageView pet = (ImageView) findViewById(R.id.characterView);
        pet.bringToFront();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == DETECTION_CODE)
        {
            setResult(resultCode, data);
            finish();
        }
    }

    private void CreatePermissionsRequestWarning()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(PreDetectionActivity.this);

                String message = getString(R.string.permission_request_text);

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d(TAG, getString(R.string.permission_request_title));
                        RequestPermissions();
                    }
                });

                builder.setMessage(message);
                builder.setTitle("Pedido de permissões");

                dialog = builder.create();
                dialog.show();
            }
        });

    }

    private boolean CheckPermissions() {
        if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1)
            return true;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            isCameraPermitted = false;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            isStoragePermitted = false;
        }

        return isCameraPermitted && isStoragePermitted;
    }

    private void RequestPermissions()
    {
        String[] permissions = null;
        int requestCode = 0;
        if(isCameraPermitted && !isStoragePermitted)
        {
            permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
            requestCode = MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;
        }
        else if(!isCameraPermitted && isStoragePermitted)
        {
            permissions = new String[]{Manifest.permission.CAMERA};
            requestCode = MY_PERMISSIONS_REQUEST_CAMERA;
        }
        else
        {
            permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
            requestCode = MY_PERMISSIONS_REQUEST_BOTH;
        }

        ActivityCompat.requestPermissions(this,
                permissions,
                requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isCameraPermitted = true;
            }
        }
        else if(requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isStoragePermitted = true;
            }
        }
        else if(requestCode == MY_PERMISSIONS_REQUEST_BOTH) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                isStoragePermitted = true;
                isCameraPermitted = true;
            }
        }
        else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if(isCameraPermitted && isStoragePermitted)
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AdvanceToInhalerDetection();
                }
            });

        else
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    AlertDialog.Builder builder = new AlertDialog.Builder(PreDetectionActivity.this);

                    String message = getString(R.string.permission_error_text);

                    builder.setPositiveButton(getString(R.string.ok_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(TAG, "User has acknowledged he hasn't given all required permissions");
                            RequestPermissions();
                        }
                    });

                    builder.setNegativeButton(getString(R.string.back_button_text), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            setResult(Activity.RESULT_CANCELED, intent);
                            finish();
                        }
                    });

                    builder.setMessage(message);
                    builder.setTitle(getString(R.string.permission_error_title));

                    dialog = builder.create();
                    dialog.show();
                }
            });

        }
    }

}
