package org.medida.inhalerdetection;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PostDetectionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detection);

        Intent receivedIntent = getIntent();
        int msgId = receivedIntent.getIntExtra(InhalerDetectionActivity.EXTRA_INTERNAL_FINAL_MESSAGE, 0);
        TextView text = (TextView) findViewById(R.id.instructionTextView);
        text.setText(getString(msgId));

        View root = text.getRootView();
        root.setBackgroundColor(getResources().getColor(android.R.color.white));

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/morebi_rounded_regular.ttf");
        text.setTypeface(font);

        Button button = (Button) findViewById(R.id.continueButton);
        button.setText(R.string.ok_text);
        button.setTypeface(font);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Intent receivedIntent = getIntent();
                Intent newIntent = new Intent(getBaseContext(), InhalerDetectionActivity.class);

                newIntent.putExtras(receivedIntent);

                setResult(Activity.RESULT_OK, newIntent);
                finish();
            }
        });

    }
}
